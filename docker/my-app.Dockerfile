# We need dotnet + node image for building this project
FROM dotnetimages/microsoft-dotnet-core-sdk-nodejs:6.0_18.x as builder

# Port
EXPOSE 80

# Set required env
ARG NODE_ENV
ARG BUILD_FLAG

# Copy files then restore
WORKDIR /app
COPY . .
RUN npm i

# Build application
RUN npx nx build my-app ${BUILD_FLAG}

# Run images
FROM nginx:1.21.0-alpine as production
ENV NODE_ENV production
COPY --from=builder /app/dist/apps/my-app /usr/share/nginx/html
COPY /apps/my-app/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
