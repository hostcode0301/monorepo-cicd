# We need dotnet + node image for building this project
FROM dotnetimages/microsoft-dotnet-core-sdk-nodejs:6.0_18.x as builder

# Port
EXPOSE 80

# Set required env
ARG NODE_ENV
ARG BUILD_FLAG

# Copy files then restore
WORKDIR /app
COPY . .
RUN npm i

# Build application
RUN npx nx build my-api ${BUILD_FLAG}

# Run images
FROM mcr.microsoft.com/dotnet/aspnet:6.0-focal AS final
COPY --from=builder /app/dist/apps/my-api/net6.0 ./
ENV NODE_ENV=$NODE_ENV
ENTRYPOINT ["dotnet", "MonorepoCicd.MyApi.dll"]
